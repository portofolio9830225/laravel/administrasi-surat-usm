function getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
        var img = new Image();
        img.setAttribute("crossOrigin", "anonymous");
        img.onload = () => {
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            var dataURL = canvas.toDataURL("image/png");
            resolve(dataURL);
        };
        img.onerror = (error) => {
            reject(error);
        };
        img.src = url;
    });
}

async function handlePDF() {
    var docDefinition2 = {
        content: [
            {
                layout: "noBorders",
                style: "tableExample",
                table: {
                    widths: ["auto", "*"],
                    headerRows: 1,
                    body: [
                        [
                            {
                                image: await this.getBase64ImageFromURL(logo),
                                width: 70,
                            },
                            [
                                {
                                    alignment: "center",
                                    characterSpacing: 1,
                                    bold: true,
                                    text: "YAYASAN ALUMNI UNIVERSITAS DIPONEGORO",
                                },
                                {
                                    alignment: "center",
                                    characterSpacing: 2,
                                    bold: true,
                                    fontSize: 15,
                                    text: "UNIVERSITAS SEMARANG",
                                },
                                {
                                    alignment: "center",
                                    // characterSpacing: 2,
                                    bold: true,
                                    fontSize: 15,
                                    text: "FAKULTAS TEKNOLOGI INFORMASI DAN KOMUNIKASI",
                                },
                                {
                                    alignment: "center",
                                    // characterSpacing: 2,
                                    fontSize: 9,
                                    text: "Sekretariat : Jl. Soekarno Hatta Tlogosari Semarang 50196 Telp. (024) 6702757 Fax. (024) 6702272",
                                },
                                {
                                    alignment: "center",
                                    // characterSpacing: 2,
                                    fontSize: 9,
                                    text: "Website : www.usm.ac.id Email : univ_smg@usm.ac.id",
                                },
                            ],
                        ],
                    ],
                },
            },
            {
                columns: [
                    {
                        width: "*",
                        alignment: "center",
                        background: "black",
                        text: "Star-sized columns have always equal widths, so if we defne 3 of those, it'll look like this (makeStar-sized columns have always equal widths, so if we defne 3 of those, it'll look like this (makeStar-sized columns have always equal widths, so if we defne 3 of those, it'll look like this (makeStar-sized columns have always equal widths, so if we defne 3 of those, it'll look like this (make",
                        fontSize: 3,
                        margin: [0, 0, 0, 5],
                    },
                ],
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 30],
                table: {
                    widths: [50, "auto", "auto"],
                    headerRows: 1,
                    body: [
                        ["Nomor", ":", `${no_surat}/USM. H5.FTIK/I/2022`],
                        ["Lamp.", ":", "-"],
                        [
                            "Hal.",
                            ":",
                            {
                                text: "Permohonan Magang",
                                bold: true,
                            },
                        ],
                    ],
                },
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 20],
                table: {
                    widths: [50, "auto"],
                    headerRows: 1,
                    body: [
                        ["", "Kepada Yth."],
                        ["", `Pimpinan ${pimpinan}`],
                        ["", alamat],
                    ],
                },
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 15],
                table: {
                    widths: [50, "auto"],
                    headerRows: 1,
                    body: [
                        ["", "Dengan Hormat,"],
                        [
                            "",
                            {
                                alignment: "justify",
                                text: "Mengacu pada Undang Undang No.20 Tahun 2003 tentang Sistem Pendidikan Nasional bahwa mahasiswa Fakultas Teknologi Informasi dan Komunikasi, Universitas Semarang pada semester VI (enam) diwajibkan melaksanakan Kerja Praktek untuk memperoleh pengalaman kerja nyata.",
                            },
                        ],
                        [
                            "",
                            {
                                alignment: "justify",
                                text: "Sehubungan dengan hal tersebut, sekiranya memungkinkan kami mohon bantuan Bapak/Ibu agar mahasiswa kami:",
                            },
                        ],
                    ],
                },
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 20],
                table: {
                    widths: [80, 100, "auto", "auto"],
                    headerRows: 1,
                    body: [
                        [
                            "",
                            { bold: true, text: "Nama" },
                            { bold: true, text: ":" },
                            { bold: true, text: nama },
                        ],
                        [
                            "",
                            { bold: true, text: "NIM" },
                            { bold: true, text: ":" },
                            { bold: true, text: nim },
                        ],
                        [
                            "",
                            { bold: true, text: "Program Studi" },
                            { bold: true, text: ":" },
                            { bold: true, text: prodi },
                        ],
                    ],
                },
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 15],
                table: {
                    widths: [50, "auto"],
                    headerRows: 1,
                    body: [
                        [
                            "",
                            {
                                alignment: "justify",
                                text: `Dapat diijinkan untuk melaksanakan Kerja Praktek di ${pimpinan} dengan judul/materi  selama 1 (Satu) bulan. Mengenai waktu pelaksanaan kami menunggu konfirmasi Bapak/Ibu.`,
                            },
                        ],
                        [
                            "",
                            {
                                alignment: "justify",
                                text: "Demikian permohonan ini kami sampaikan, atas perhatian dan bantuannya kami ucapkan terimakasih.",
                            },
                        ],
                    ],
                },
            },
            {
                layout: "noBorders",
                margin: [0, 0, 0, 15],
                table: {
                    widths: [50, "*", "*"],
                    headerRows: 1,
                    body: [
                        [
                            "",
                            {
                                alignment: "center",
                                text: "Mengetahui",
                            },
                            {
                                alignment: "center",
                                text: "Ketua Jurusan",
                            },
                        ],
                        [
                            "",
                            {
                                alignment: "center",
                                text: "Wakil Dekan I\n\n",
                            },
                            {
                                alignment: "center",
                                text: "Teknologi Informasi",
                            },
                        ],
                        [
                            "",
                            {
                                layout: "noBorders",
                                table: {
                                    widths: ["auto", "*"],
                                    headerRows: 1,
                                    body: [
                                        [
                                            {
                                                qr: "FTIK DIGITAL SIGNATURE",
                                                fit: "50",
                                            },
                                            {
                                                color: "grey",
                                                text: "\nFTIK DIGITAL SIGNATURE",
                                            },
                                        ],
                                    ],
                                },
                            },
                            {
                                layout: "noBorders",
                                table: {
                                    widths: ["auto", "*"],
                                    headerRows: 1,
                                    body: [
                                        [
                                            {
                                                qr: "FTIK DIGITAL SIGNATURE",
                                                fit: "50",
                                            },
                                            {
                                                color: "grey",
                                                text: "\nFTIK DIGITAL SIGNATURE",
                                            },
                                        ],
                                    ],
                                },
                            },
                        ],
                        [
                            "",
                            {
                                alignment: "center",
                                text: "\nFajriannoor Fanani, S.Sos., M.I.Kom.",
                                decoration: "underline",
                            },
                            {
                                alignment: "center",
                                text: "\nNurtriana Hidayati, S.Kom., M.Kom.",
                                decoration: "underline",
                            },
                        ],
                        [
                            "",
                            {
                                alignment: "center",
                                text: "NIS. 06557000606017",
                            },
                            {
                                alignment: "center",
                                text: "NIS. 06557003102131",
                            },
                        ],
                    ],
                },
            },
        ],
        footer: {
            columns: [
                {
                    text: "Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh E-Letter FTIK USM",
                    color: "#A0A0A0",
                    margin: [25, 0, 20, 0],
                    fontSize: 9.5,
                },
            ],
        },
        styles: {
            tableExample: {
                margin: [0, 0, 0, 10],
            },
        },
    };

    const win = window.open("", "_blank");

    pdfMake.createPdf(docDefinition2).open(win);
}

function handleWord() {
    // alert(123);
    Swal.fire({
        icon: "warning",
        title: "Masih Tahap Pengembangan",
    });
}
