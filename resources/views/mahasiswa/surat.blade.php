@extends('layout.main')
@section('content')
    <div class="w-100 d-flex justify-content-between align-items-center">
        <h2>Surat Masuk</h2>
        <div>
            <a href="{{ route('surat.tambah') }}" class="btn btn-sm btn-outline-primary"> <i
                    class="fas fa-plus"></i>&nbsp;Ajukan Surat</a>
        </div>
    </div>
    <div class="table-responsive w-100">
        <table class="table table-bordered table-striped" id="datatables" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 30px;">#</th>
                    <th>No Surat</th>
                    <th>Perihal</th>
                    <th>Status</th>
                    <th>Tanggal Dibuat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('surat') !!}',
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nomor',
                        name: 'nomor'
                    },
                    {
                        data: 'perihal',
                        name: 'perihal'
                    },
                    {
                        data: 'status_pengajuan',
                        name: 'status_pengajuan'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });

        function handleDelete(id) {
            Swal.fire({
                title: 'Yakin Delete?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete!!!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = window.location + '/delete/' + id
                }
            })
        }
    </script>
@endsection
