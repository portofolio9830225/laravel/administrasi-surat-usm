@extends('layout.main')
@section('content')
    <h2>Informasi Surat</h2>
    <select style="width: 100%;" id="select2" aria-label="Default select example">
        <option value="">Pilih ---</option>
        @foreach ($informasi as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
        @endforeach
    </select>
    <div class="mt-3" style="display: none;" id="informasi">
        <table class="table">
            <tr>
                <th width="150px">Deskripsi</th>
                <td width="1">:</td>
                <td id="deskripsi">...</td>
            </tr>
            <tr>
                <th>File</th>
                <td width="1">:</td>
                <td id="file">...</td>
            </tr>
        </table>
    </div>
@endsection
@section('css')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#select2').select2();
        });

        $('#select2').change(function() {
            // console.log($(this).val());
            if ($(this).val() != '') {
                $('#deskripsi').html('...');
                $('#file').html('...');
                $('#informasi').show();
                $.ajax('/surat/kategori/detail/' + $(this).val(), {
                    success: function(data) {
                        $('#deskripsi').html(data.deskripsi);
                        $('#file').html(
                            `<button type="button" class="btn btn-sm btn-info" onclick="handleDownload(${data.id})"><i class="fas fa-file-download"></i>&nbspDownload</button>`
                        );
                    },
                });
            } else {
                $('#informasi').hide();
                $('#deskripsi').html('...');
                $('#file').html('...');
            }
        })

        function handleDownload(id) {
            window.location.href = '/surat/kategori/download/' + id;
        }
    </script>
@endsection
