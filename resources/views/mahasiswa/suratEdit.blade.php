@extends('layout.main')
@section('content')
    <div class="row justify-content-center mb-5">
        <div class="col-md-8 mb-5">
            <div class="card shadow">
                <div class="card-body">
                    <form method="POST" action="{{ route('surat.update') }}" enctype="multipart/form-data">
                        @csrf
                        @method('patch')
                        <input type="hidden" name="nama_jns_surat" value="{{ $surat->perihal }}">
                        <input type="hidden" name="id_surat" value="{{ $surat->id }}">
                        <div class="w-100 pb-2 border-bottom mb-3">
                            <i class="fas fa-folder-plus"></i>&nbsp;Edit Surat
                        </div>
                        <div class="row mb-3">
                            <label for="Penerima" class="col-sm-3 col-form-label">Penerima</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="Penerima" value="TATA USAHA FTIK USM"
                                    readonly />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="perihal" class="col-sm-3 col-form-label">Perihal</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="perihal" required name="perihal" disabled>
                                    <option selected>{{ $surat->perihal }}</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="row mb-3 pb-2 border-bottom">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Dokumen Surat</label>
                            <div class="col-sm-9">
                                <input type="file" name="file" required accept=".pdf">
                            </div>
                        </div> --}}
                        <div class="mb-3">
                            <h6 class="mb-3">Data Surat Kelengkapan</h6>
                            @foreach ($surat->surat_kelengkapan as $item)
                                <div class="row mb-3">
                                    <label for="{{ $item->key }}"
                                        class="col-sm-3 col-form-label">{{ str($item->key)->headline() }}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="{{ $item->value }}"
                                            name="key[]" />
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="row mb-3">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Dokumen Ajuan</label>
                            <div class="col-sm-9">
                                <input type="file" name="file" required accept=".pdf">
                            </div>
                        </div>
                        <div class="d-flex justify-content-between pt-2 border-top">
                            <div>
                                <a href="{{ route('surat') }}" class="btn btn-sm btn-outline-secondary"><i
                                        class="fas fa-undo"></i>&nbsp;Kembali</a>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fas fa-save"></i>&nbsp;Kirim
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-5">
            <div class="card border-success mb-3">
                <div class="card-body text-success">
                    <div class="mb-3">
                        <p class="card-text mb-1">Catatan TU :</p>
                        <textarea class="form-control" rows="3" placeholder="Beri catatan..." disabled>{{ $surat->catatan }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
