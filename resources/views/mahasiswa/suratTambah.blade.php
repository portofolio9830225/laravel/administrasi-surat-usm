@extends('layout.main')
@section('content')
    <livewire:mahasiswa.surat-tambah />
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script>
        $('#perihal').change(function() {
            // console.log(this.value);
            if (this.value != '') {
                $.ajax(`/surat/informasi?nama=${this.value}`, {
                    success: function(data) {
                        // console.log(data);
                        let html = data.deskripsi;
                        let html2 =
                            ` <a href="/surat/kategori/download/${data.id}" class="badge rounded-pill bg-primary text-decoration-none text-white">Download Formulir ${data.deskripsi}</a>`;
                        $('#informasisurat').show().html(html);
                        // $('#downloadFile').show().html(html2);
                    }
                });
            } else {
                $('#informasisurat').hide();
            }
        })
    </script>
@endsection
