<div>
    <div class="w-100 bg-utama" style="height: 100vh;">
        <div class="divider"></div>
        <div class="w-100 py-5 px-4 text-center text-white">
            <h1>Selamat Datang di Administrasi Surat FTIK USM</h1>
        </div>
        <div class="px-5">
            @if ($layout == 'login')
                <div class="w-100 d-flex justify-content-evenly align-items-center">
                    <img src="{{ asset('file/usm2.png') }}" alt="" style="width: 300px" class="d-none d-md-block" />
                    <div>
                        <div class="card">
                            <div class="card-body">
                                <h2 class="mb-3 text-center text-uppercase fw-bold">
                                    Login
                                </h2>
                                <hr />
                                @if (session('msg'))
                                    <div class="alert alert-danger">
                                        {{ session('msg') }}
                                    </div>
                                @endif
                                <form wire:submit.prevent="submit" autocomplete="off">
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="NIM" wire:model="nim" />
                                        @error('nim')
                                            <div class="form-text text-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="mb-3">
                                        <input type="password" class="form-control" placeholder="Password"
                                            wire:model="password" />
                                        @error('password')
                                            <div class="form-text text-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <button type="submit" class="btn btn-primary w-100">
                                        Login
                                    </button>
                                    <hr>
                                    <div class="btn btn-outline-primary w-100" wire:click="handleLayout">Registrasi Akun
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="container">
                    <div class="card">
                        <div class="card-body">
                            <form action="#" method="post" wire:submit.prevent="register">
                                @if ($errors->any())
                                    <div class="alert alert-danger text-uppercase text-center">
                                        {{ $errors->first() }} !!!
                                    </div>
                                @endif
                                <div class="row mb-4">
                                    <div class="col-md-4">
                                        <div class="mb-3">
                                            <label for="Nama" class="form-label">Nama</label>
                                            <input type="text" class="form-control" id="Nama" placeholder="Nama"
                                                name="nama" required wire:model="namaRegister">
                                        </div>
                                        <div class="mb-3">
                                            <label for="Nim" class="form-label">NIM</label>
                                            <input type="text" class="form-control" id="Nim" placeholder="Nim"
                                                name="nim" required wire:model="nimRegister">
                                            <div id="emailHelp" class="form-text">contoh : G.211.40.1902</div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-3">
                                            <label for="Nama" class="form-label">Jenis Kelamin</label>
                                            <select class="form-select" name="jenis_kelamin" required
                                                wire:model="jenisKelamin">
                                                <option value="" selected>-- Pilih --</option>
                                                <option value="Laki-Laki">Laki-Laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="Nama" class="form-label">Program Studi</label>
                                            <select class="form-select" name="program_studi_id" required
                                                wire:model="prodi_id">
                                                <option value="" selected>-- Pilih --</option>
                                                @foreach ($prodi as $item)
                                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="mb-3">
                                            <label for="Nama" class="form-label">Jenis Mahasiswa</label>
                                            <select class="form-select" name="jenis_mhs" required
                                                wire:model="jenis_mhs">
                                                <option value="" selected>-- Pilih --</option>
                                                <option value="Pagi">Pagi</option>
                                                <option value="Sore">Sore</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="mb-3">
                                            <label for="email" class="form-label">Email</label>
                                            <input type="email" class="form-control" id="email"
                                                placeholder="Email" name="email" required wire:model="email">
                                        </div>
                                        <div class="mb-3">
                                            <label for="password" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="password"
                                                placeholder="Password" name="password" required
                                                wire:model="password2">
                                        </div>
                                        <div class="mb-3">
                                            <label for="password_konfirmasi" class="form-label">Konfirmasi
                                                Password</label>
                                            <input type="password" class="form-control" id="password_konfirmasi"
                                                placeholder="Konfirmasi Password" name="password_konfirmasi" required
                                                wire:model="password_konfirmasi">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 mb-2">Register</button>
                                <div class="btn btn-outline-primary w-100" wire:click="handleLayout">Login</div>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
