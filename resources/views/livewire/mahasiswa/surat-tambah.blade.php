<div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-body">
                    <form method="POST" action="{{ route('surat.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="w-100 pb-2 border-bottom mb-3">
                            <i class="fas fa-folder-plus"></i>&nbsp;Ajuan
                            Surat
                        </div>
                        <div class="row mb-3">
                            <label for="Penerima" class="col-sm-3 col-form-label">Penerima</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="Penerima" value="TATA USAHA FTIK USM"
                                    readonly />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="perihal" class="col-sm-3 col-form-label">Perihal</label>
                            <div class="col-sm-9">
                                <select class="form-select" id="perihal" required name="perihal" wire:model="perihal">
                                    <option value="">Pilih Surat ---</option>
                                    @foreach ($surat as $item)
                                        <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                                @if ($perihal != '')
                                    <div class="form-text text-secondary">{{ $deskripsi }}</div>
                                @endif
                            </div>
                        </div>
                        @if (count($isi) > 0)
                            <div class="mb-3">
                                <h6 class="mb-3">Data Surat Kelengkapan</h6>
                                @foreach ($isi as $item)
                                    <div class="row mb-3">
                                        <label for="{{ $item->key }}"
                                            class="col-sm-3 col-form-label">{{ str($item->key)->headline() }}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" name="key[]"
                                                placeholder="{{ str($item->key)->headline() }}" required />
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="row mb-3">
                                <label for="inputEmail3" class="col-sm-3 col-form-label">Dokumen Ajuan</label>
                                <div class="col-sm-9">
                                    <input type="file" name="file" required accept=".pdf">
                                </div>
                            </div>
                        @endif
                        <div class="d-flex justify-content-between pt-2 border-top">
                            <div>
                                <a href="{{ route('surat') }}" class="btn btn-sm btn-outline-secondary"><i
                                        class="fas fa-undo"></i>&nbsp;Kembali</a>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fas fa-save"></i>&nbsp;Kirim
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
