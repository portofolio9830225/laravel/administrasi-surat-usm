<div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-body">
                    <div class="w-100 d-flex justify-content-between pb-2 border-bottom mb-3">
                        <div>
                            <i class="fas fa-folder-plus"></i>&nbsp;Detail Surat
                        </div>
                        <span class="fw-light">{{ $surat->created_at->diffForHumans() }}</span>
                    </div>
                    <div class="row mb-3">
                        <label for="Penerima" class="col-sm-3 col-form-label">Penerima</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="Penerima" value="TATA USAHA FTIK USM"
                                readonly />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="perihal" class="col-sm-3 col-form-label">Perihal</label>
                        <div class="col-sm-9">
                            <select class="form-select" id="perihal" disabled>
                                <option>{{ $surat->perihal }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-3 col-form-label">Status</label>
                        <div class="col-sm-9">
                            @if ($surat->status_pengajuan == 'Diterima TU' || $surat->status_pengajuan == 'Selesai')
                                <span class="badge bg-success">{{ $surat->status_pengajuan }}</span>
                            @elseif($surat->status_pengajuan == 'Ditolak TU')
                                <span class="badge bg-danger">{{ $surat->status_pengajuan }}</span>
                            @elseif($surat->status_pengajuan == 'Proses Paraf')
                                <span class="badge bg-primary">{{ $surat->status_proses_paraf }}</span>
                            @else
                                <span class="badge bg-warning text-dark">{{ $surat->status_pengajuan }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="mb-3">
                        <h6 class="mb-3">Data Surat Kelengkapan</h6>
                        @foreach ($surat->surat_kelengkapan as $item)
                            <div class="row mb-3">
                                <label for="{{ $item->key }}"
                                    class="col-sm-3 col-form-label">{{ str($item->key)->headline() }}</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" value="{{ $item->value }}" readonly />
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="row mb-3">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Dokumen Ajuan</label>
                        <div class="col-sm-9">
                            <a href="{{ route('surat.review_pdf', [$surat->id]) }}"
                                class="btn btn-sm btn-outline-secondary" target="_blank">
                                <i class="fas fa-search"></i>&nbsp;Review PDF
                            </a>
                        </div>
                    </div>
                    <div class="d-flex justify-content-between pt-2 border-top">
                        <div>
                            <a href="{{ route('surat') }}" class="btn btn-outline-secondary btn-sm"><i
                                    class="fas fa-undo"></i>&nbsp;Kembali</a>
                        </div>
                        @if ($surat->status_pengajuan == 'Selesai')
                            <div class="d-flex">
                                <button class="btn btn-outline-danger btn-sm" onclick="handlePDF()">Download
                                    Surat</button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (in_array($surat->status_pengajuan, ['Ditolak TU']))
            <div class="col-md-4 mb-5">
                <div class="card border-success mb-3">
                    <div class="card-body text-success">
                        <p class="card-text mb-1">Catatan :</p>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{ $surat->catatan }}</textarea>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
