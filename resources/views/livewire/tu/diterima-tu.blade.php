<div>
    <div class="mb-2">
        @if ($surat->status_pengajuan != 'Selesai')
            <p class="card-text mb-1">Request Paraf :</p>
            <select class="form-select form-select-sm mb-2" wire:model="ajuan_ttd">
                <option value="">Pilih ---</option>
                <option value="Dekan">Dekan</option>
                <option value="KAPROGDI TI">KAPROGDI TI</option>
                <option value="KAPROGDI SI">KAPROGDI SI</option>
                <option value="KAPROGDI ILKOM">KAPROGDI ILKOM</option>
            </select>
        @endif
        @if ($ajuan_ttd != '')
            <button type="button" class="btn btn-sm btn-primary w-100" wire:click="mintaParaf">
                Kirim
            </button>
        @endif
        @if (count($ttd) > 0)
            @if ($surat->status_pengajuan != 'Selesai')
                <hr />
            @endif
            @foreach ($ttd as $item)
                <p class="card-text mb-1">TTD {{ $item->user->jabatan }} :
                    @if ($item->ttd == '0')
                        <span class="badge rounded-pill bg-warning text-dark">Proses
                            Paraf</span>
                        <span class="badge rounded-pill bg-danger text-dark text-white" style="cursor: pointer;"
                            wire:click="deleteParaf({{ $item->id }})"><i class="fas fa-times"></i></span>
                    @else
                        <span class="badge rounded-pill bg-success">ACC</span>
                    @endif
                </p>
            @endforeach
        @endif
        @if ($is_pdf)
            @if ($surat->status_pengajuan != 'Selesai')
                <div class="my-3">
                    <input type="text" class="form-control" wire:model="nomor" placeholder="No Surat" />
                </div>
                <button class="btn btn-sm btn-outline-danger w-100" wire:click="generatePdf">Generate</button>
            @endif
        @endif
    </div>
</div>
