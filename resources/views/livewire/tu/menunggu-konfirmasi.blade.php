<div>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">Status Pengajuan</label>
        <select class="form-select" aria-label="Default select example" required wire:model="paraf">
            <option value="">Pilih ---</option>
            <option value="Diterima TU">
                Diterima
            </option>
            <option value="Revisi">
                Revisi
            </option>
            <option value="Ditolak TU">
                Ditolak
            </option>
        </select>
    </div>
    @if (in_array($paraf, ['Revisi', 'Ditolak TU']))
        <div class="mb-3">
            <p class="card-text mb-1">Catatan :</p>
            <textarea class="form-control" rows="3" placeholder="Beri catatan..." wire:model="catatan"></textarea>
        </div>
        {{-- @elseif ($paraf == 'Diterima TU')
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">No Surat</label>
            <input type="text" class="form-control" wire:model="nomor" placeholder="No Surat" />
        </div> --}}
    @endif
    <button type="button" class="btn btn-sm btn-primary" wire:click="saveAjuanSurat">
        @if ($isProses)
            Loading...
        @else
            Submit
        @endif
    </button>
</div>
