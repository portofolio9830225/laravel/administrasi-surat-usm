<div>
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">Status Pengajuan</label>
        <select class="form-select" aria-label="Default select example" required wire:model="ajuan_paraf" id="ajuanParaf">
            <option value="">Pilih ---</option>
            <option value="Diterima">
                Diterima
            </option>
            <option value="Revisi">
                Revisi
            </option>
        </select>
    </div>
    @if ($ajuan_paraf == 'Revisi')
        <form wire:submit.prevent="submit">
            <p class="card-text mb-1">Beri Catatan :</p>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Beri catatan..."
                wire:model="catatan" required></textarea>
            <button type="submit" class="btn btn-sm btn-primary mt-2 w-100">
                Submit
            </button>
        </form>
    @endif
</div>
