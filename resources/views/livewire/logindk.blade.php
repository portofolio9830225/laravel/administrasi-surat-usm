<div>
    <div class="w-100 bg-utama" style="height: 100vh">
        <div class="divider"></div>
        <div class="w-100 mt-5 px-3 text-center text-white">
            <h1>Selamat Datang di Administrasi Surat FTIK USM</h1>
            <div class="row">
                <div class="col-md-4 m-auto p-5">
                    <div class="card" style="border: 2px solid #ff5722">
                        <div class="card-header text-center">
                            <img src="{{ asset('file/usm2.png') }}" alt="" style="width: 200px"
                                class="p-3" />
                        </div>
                        <div class="card-body">
                            @if (session('msg'))
                                <div class="alert alert-danger">
                                    {{ session('msg') }}
                                </div>
                            @endif
                            <form wire:submit.prevent="submit" autocomplete="off">
                                <div class="mb-3">
                                    <input type="email" class="form-control" placeholder="Email"
                                        wire:model="email" />
                                    @error('email')
                                        <div class="form-text text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-3">
                                    <input type="password" class="form-control" placeholder="Password"
                                        wire:model="password" />
                                    @error('password')
                                        <div class="form-text text-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <button type="submit" class="btn btn-primary btn-orange w-100"
                                    style="background-color: #ff5722; border-color: #ff5722;">
                                    Submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
