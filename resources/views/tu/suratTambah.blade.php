@extends('layout.main')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-body">
                    <form method="POST" action="{{ route('surat.store_dosen') }}">
                        @csrf
                        <div class="w-100 pb-2 border-bottom mb-3">
                            <i class="fas fa-folder-plus"></i>&nbsp;Ajuan
                            Surat
                        </div>
                        <div class="row mb-3">
                            <label for="penerima" class="col-sm-3 col-form-label">Penerima</label>
                            <div class="col-sm-9">
                                <select name="penerima" id="penerima" class="form-select" selected>
                                    <option value="">Pilih ---</option>
                                    @foreach ($dosen as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="perihal" class="col-sm-3 col-form-label">Nomor</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nomor" name="nomor" required
                                    placeholder="Nomor...">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="perihal" class="col-sm-3 col-form-label">Perihal</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="perihal" name="perihal" required
                                    placeholder="Perihal...">
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>
                                <a href="{{ url()->previous() }}" class="btn btn-sm btn-outline-secondary"><i
                                        class="fas fa-undo"></i>&nbsp;Kembali</a>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="fas fa-save"></i>&nbsp;Kirim
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
