@extends('layout.main')
@section('content')
    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link {{ $active == 'Surat Mahasiswa' ? 'active' : '' }}" href="{{ route('surat') }}">Surat
                Mahasiswa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'Surat Dosen' ? 'active' : '' }}" href="{{ route('surat.dosen') }}">Surat
                Dosen</a>
        </li>
    </ul>
    <div class="w-100 d-flex justify-content-between align-items-center">
        <h2>Surat Dosen</h2>
        <div>
            {{-- <a href="{{ route('surat.tambah.dosen') }}" class="btn btn-sm btn-outline-primary"><i
                    class="fas fa-plus"></i>&nbsp;Ajukan Surat</a> --}}
            <button type="button" class="btn btn-sm btn-outline-primary" data-bs-toggle="modal"
                data-bs-target="#exampleModal">
                <i class="fas fa-plus"></i>&nbsp;Ajukan Surat
            </button>
        </div>
    </div>
    <div class="table-responsive w-100 mb-5">
        <table class="table table-bordered table-striped" id="datatables" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 30px;">#</th>
                    <th>No Surat</th>
                    <th>Nama</th>
                    <th>Perihal</th>
                    <th>Tanggal Dibuat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-folder-plus"></i>&nbsp;Ajuan
                        Surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('surat.store_dosen') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Penerima</label>
                            <select name="penerima" id="penerima" class="form-select" selected>
                                <option value="">Pilih ---</option>
                                @foreach ($dosen as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="nomor" class="form-label">Nomor</label>
                            <input type="text" class="form-control" id="nomor" name="nomor" required
                                placeholder="Nomor">
                        </div>
                        <div class="mb-3">
                            <label for="perihal" class="form-label">Perihal</label>
                            <input type="text" class="form-control" id="perihal" name="perihal" required
                                placeholder="Perihal">
                        </div>
                        <div class="mb-3">
                            <label for="formFile" class="form-label">File</label>
                            <input class="form-control" type="file" id="formFile" name="file" required
                                accept=".pdf">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">
                            <i class="fas fa-save"></i>&nbsp;Kirim
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('surat.dosen') !!}',
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nomor',
                        name: 'nomor'
                    },
                    {
                        data: 'user.nama',
                        name: 'user.nama'
                    },
                    {
                        data: 'perihal',
                        name: 'perihal'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });

        function handleDelete(id) {
            Swal.fire({
                title: 'Yakin Delete?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete!!!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = window.location + '/delete/' + id
                }
            });
        }
    </script>
@endsection
