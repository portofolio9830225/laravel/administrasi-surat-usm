@extends('layout.main')
@section('content')
    <div class="container mt-3">
        <h1 class="fw-bold mb-3">Sistem Administrasi Surat FTIK USM</h1>
        <div class="row py-3 mb-3">
            <div class="col-md-6 mb-5">
                <div class="card border-primary">
                    <div class="card-body position-relative">
                        <canvas id="myChart2"></canvas>
                        @if ($ditolak == 0 && $diterima == 0 && $revisi == 0 && $menunggu == 0 && $selesai == 0 && $paraf == 0)
                            <div class="position-absolute top-50 start-50 translate-middle">
                                <p>No data available...</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-5">
                <div class="card ">
                    <div class="card-body">
                        <h5 class="fw-normal">Aktifitas Kamu</h5>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th width="50px">#</th>
                                    <th>Log</th>
                                </tr>
                                @foreach ($logs as $item)
                                    <tr>
                                        <td width="50px">{{ $loop->iteration }}</td>
                                        <td>{{ $item->description }}
                                            <span
                                                style="font-size: 80%;">({{ $item->created_at->diffForHumans() }})</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        const data = {
            labels: [
                'Ditolak',
                'Revisi',
                'Diterima',
                'Menunggu Konfirmasi',
                'Proses Paraf',
                'Selesai',
            ],
            datasets: [{
                label: 'Status Pengajuan Surat',
                data: [{{ $ditolak }}, {{ $revisi }}, {{ $diterima }}, {{ $menunggu }},
                    {{ $paraf }},
                    {{ $selesai }}
                ],
                backgroundColor: [
                    'rgb(255, 0, 0)',
                    'rgb(246, 233, 0)',
                    'rgb(0,128,0)',
                    'rgb(255,165,0)',
                    'rgb(219, 121, 0)',
                    'rgb(52, 235, 61)',
                ],
                hoverOffset: 5
            }]
        };

        const config2 = {
            type: 'doughnut',
            data: data,
        };

        const myChart2 = new Chart(
            document.getElementById('myChart2'),
            config2
        );
    </script>
@endsection
