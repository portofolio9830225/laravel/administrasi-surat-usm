@extends('layout.main')
@section('content')
    <livewire:tu.surat-detail :idsurat="$id" :pdf="$pdf" />
@endsection
@section('js')
    <script>
        const link = '<?= $link ?>';
        const pimpinan = '{{ $surat->surat_kelengkapan[0]->value }}';
        const alamat = '{{ $surat->surat_kelengkapan[1]->value }}';
        const nama = '{{ $surat->user->nama }}';
        const nim = '{{ $surat->user->nim }}';
        const prodi = '{{ $surat->user->program_studi->nama }}';
        const no_surat = '{{ $surat->nomor }}';
        const logo = "{{ asset('LOGOUSMJAYA.png') }}";
    </script>
    <script src="{{ asset('js/qrcode.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.3.0-beta.2/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.3.0-beta.2/fonts/Roboto.min.js"></script>
    <script src="{{ asset('js/surat-detail4.js') }}"></script>
@endsection
