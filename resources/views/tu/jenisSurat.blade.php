@extends('layout.main')
@section('content')
    {{-- <livewire:tu.jenis-surat /> --}}
    <div class="w-100 d-flex justify-content-between align-items-center">
        <h2>Kategori Surat</h2>
        <div>
            <button type="button" class="btn btn-sm btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fas fa-plus"></i>&nbsp;Tambah
            </button>
            <a href="{{ route('surat') }}" class="btn btn-sm btn-outline-secondary"><i
                    class="fas fa-undo"></i>&nbsp;Kembali</a>

            <!-- Tambah Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Jenis Surat</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form method="post" action="{{ route('surat.jenis_store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                <div class="mb-3">
                                    <label for="nama" class="form-label">Nama</label>
                                    <input type="text" class="form-control" id="nama" required name="nama">
                                </div>
                                <div class="mb-3">
                                    <label for="deskripsi" class="form-label">Deskripsi</label>
                                    <textarea name="deskripsi" class="form-control" id="deskripsi" rows="5" required></textarea>
                                </div>
                                {{-- <div class="mb-3">
                                    <label for="formFile" class="form-label">File</label>
                                    <input class="form-control" type="file" id="formFile" name="file" required
                                        accept=".pdf">
                                </div> --}}
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="table-responsive w-100 mb-5">
        <table class="table table-bordered table-striped" id="datatables" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 30px;">#</th>
                    <th>Nama</th>
                    <th>Deskripsi</th>
                    {{-- <th>File</th> --}}
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Edit Jenis Surat</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="post" action="{{ route('surat.jenis_update') }}" enctype="multipart/form-data">
                    @method('patch')
                    @csrf
                    <input type="hidden" name="id" id="idEdit">
                    <div class="modal-body">
                        <h2 class="w-100 text-center" id="loading">Loading...</h2>
                        <div id="editItem" style="display: none;">
                            <div class="mb-3">
                                <label for="nama" class="form-label">Nama</label>
                                <input type="text" class="form-control" id="namaEdit" required name="nama"
                                    value="">
                            </div>
                            <div class="mb-3">
                                <label for="deskripsi" class="form-label">Deskripsi</label>
                                <textarea name="deskripsi" class="form-control" id="deskripsiEdit" rows="5" required></textarea>
                            </div>
                            {{-- <div class="mb-3">
                                <label for="formFile" class="form-label">File</label>
                                <input class="form-control" type="file" id="formFile" name="file">
                            </div> --}}
                        </div>
                    </div>
                    <div class="modal-footer" id="modalEditFooter" style="display: none;">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('surat.jenis') !!}',
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'deskripsi',
                        name: 'deskripsi'
                    },
                    // {
                    //     data: 'file',
                    //     name: 'file',
                    //     orderable: false,
                    //     searchable: false
                    // },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });

        function handleDelete(id) {
            Swal.fire({
                title: 'Yakin Delete?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Delete!!!'
            }).then((result) => {
                if (result.isConfirmed) {
                    window.location.href = window.location + '/delete/' + id
                }
            })
        }

        function handleEdit(id) {
            // console.log(id);
            $('#editModal').modal('show');
            $.ajax('/surat/kategori/detail/' + id, {
                success: function(data) { // success callback function
                    // console.log(data);
                    $('#loading').hide();
                    $('#editItem').show();
                    $('#idEdit').val(data.id);
                    $('#namaEdit').val(data.nama);
                    $('#deskripsiEdit').html(data.deskripsi);
                    $('#modalEditFooter').show();
                },
            });
        }
    </script>
@endsection
