<li class="nav-item">
    <a class="nav-link link-nav" aria-current="page" href="{{ route('home') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Home"><i class="fas fa-home"
            style="font-size: 20px;"></i>&nbsp;Home</a>
</li>
<li class="nav-item">
    <a class="nav-link link-nav" aria-current="page" href="{{ route('surat') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Surat"><i class="fas fa-envelope"
            style="font-size: 20px;"></i>&nbsp;Surat</a>
</li>
