<li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('home') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Home"><i
            class="fas fa-home i-orange"></i></a>
</li>
<li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('surat') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Surat"><i
            class="fas fa-envelope i-orange"></i></a>
</li>
<li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('activity_log') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Activity Log"><i
            class="fas fa-calendar-day i-orange"></i></a>
</li>
