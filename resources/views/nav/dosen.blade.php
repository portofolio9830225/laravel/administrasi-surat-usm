<li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('home') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Home"><i
            class="fas fa-home i-orange"></i></a>
</li>
<li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('surat') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Surat"><i
            class="fas fa-envelope i-orange"></i></a>
</li>
{{-- <li class="nav-item mx-3">
    <a class="nav-link" aria-current="page" href="{{ route('surat.informasi') }}" data-bs-toggle="tooltip"
        data-bs-placement="top" data-bs-custom-class="custom-tooltip" title="Informasi Surat"><i
            class="fas fa-info-circle i-orange"></i></a>
</li> --}}
