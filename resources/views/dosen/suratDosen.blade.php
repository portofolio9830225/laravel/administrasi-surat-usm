@extends('layout.main')
@section('content')
    <ul class="nav nav-tabs mb-3">
        <li class="nav-item">
            <a class="nav-link {{ $active == 'Surat Mahasiswa' ? 'active' : '' }}" href="{{ route('surat') }}">Surat
                Mahasiswa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ $active == 'Surat Dosen' ? 'active' : '' }}" href="{{ route('surat.dosen') }}">Surat
                TU</a>
        </li>
    </ul>
    <h2>Surat TU</h2>
    <div class="table-responsive w-100 mb-5">
        <table class="table table-bordered table-striped" id="datatables" style="width:100%">
            <thead>
                <tr>
                    <th style="width: 30px;">#</th>
                    <th>No Surat</th>
                    <th>Nama</th>
                    <th>Perihal</th>
                    <th>Tanggal Dibuat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css" />
@endsection
@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script>
        $(function() {
            $('#datatables').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('surat.dosen') !!}',
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nomor',
                        name: 'nomor'
                    },
                    {
                        data: 'user.nama',
                        name: 'user.nama'
                    },
                    {
                        data: 'perihal',
                        name: 'perihal'
                    },
                    {
                        data: 'created_at',
                        name: 'created_at'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });
        });
    </script>
@endsection
