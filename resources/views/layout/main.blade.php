<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css'
        integrity='sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=='
        crossorigin='anonymous' />
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css'
        integrity='sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg=='
        crossorigin='anonymous' />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('css')
    @livewireStyles

    <title>FTIK USM</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm d-none d-lg-block" style="background-color:#003a6a">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                {{-- <img src="https://pbs.twimg.com/profile_images/414450550554370049/jaFC9lXA_400x400.png" width="30"
                    height="30" class="d-inline-block align-top" alt="" /> --}}
                <img src="{{ asset('file/usm2.png') }}" width="30" height="30" class="d-inline-block align-top"
                    alt="" /> <span class="fw-bold text-white">FTIK USM</span>
            </a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    @if (Auth::user()->level == 'Mahasiswa')
                        @include('nav.mahasiswaAtas')
                    @elseif (Auth::user()->level == 'TU')
                        @include('nav.tuAtas')
                    @else
                        @include('nav.dosenAtas')
                    @endif
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" class="nav-link link-nav">
                            <i class="fas fa-sign-out-alt" style="font-size: 20px;"></i>&nbsp;Logout
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    {{-- <nav class="navbar navbar-expand-lg navbar-light shadow-sm d-none d-lg-block" style="background-color:#003a6a">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img src="https://pbs.twimg.com/profile_images/414450550554370049/jaFC9lXA_400x400.png" width="30"
                    height="30" class="d-inline-block align-top" alt="" />
                &nbsp;
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mx-auto">
                    @if (Auth::user()->level == 'Mahasiswa')
                        @include('nav.mahasiswa')
                    @elseif (Auth::user()->level == 'TU')
                        @include('nav.tu')
                    @else
                        @include('nav.dosen')
                    @endif
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="{{ route('logout') }}" class="nav-link">
                            <i class="fas fa-sign-out-alt i-orange"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav> --}}

    <div class="container mt-3">
        <div class="alert alert-info">
            <div class="w-100 d-flex justify-content-between">
                <livewire:time />
                <div>Selamat Datang {{ Auth::user()->nama }}</div>
            </div>
        </div>
    </div>

    <div class="container mt-3">
        @yield('content')
    </div>

    <!-- Bottom Navbar Mobile -->
    <nav class="navbar navbar-light bg-utama border-top navbar-expand d-lg-none d-xl-none fixed-bottom">
        <ul class="navbar-nav nav-justified w-100">
            @if (Auth::user()->level == 'Mahasiswa')
                @include('nav.mahasiswa')
            @elseif (Auth::user()->level == 'TU')
                @include('nav.tu')
            @else
                @include('nav.dosen')
            @endif
        </ul>
    </nav>

    @if (session('status'))
        <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
            <div id="liveToast" class="toast align-items-center text-white bg-success border-0" role="alert"
                aria-live="assertive" aria-atomic="true">
                <div class="d-flex">
                    <div class="toast-body">
                        {{ session('status') }}
                    </div>
                    <button type="button" class="btn-close btn-close-white me-2 m-auto" data-bs-dismiss="toast"
                        aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif

    @livewireScripts
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var toastLiveExample = document.getElementById('liveToast');
        var toast = new bootstrap.Toast(toastLiveExample)
        toast.show();
    </script>
    @yield('js')
</body>

</html>
