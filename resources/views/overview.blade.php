<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;1,500;1,700&display=swap"
        rel="stylesheet">
    <style>
        * {
            font-family: 'Roboto', sans-serif;
        }
    </style>

    <title>Overview Surat</title>
</head>

<body style="background-color: #f5f8fa;">

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-9 py-5 mt-5">
                <div class="card shadow-sm">
                    <div class="card-header py-3 fs-3 bg-white fw-bold">
                        Overview
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <img src="https://pbs.twimg.com/profile_images/414450550554370049/jaFC9lXA_400x400.png"
                                        alt="" style="width: 300px;" class="me-3">
                                    <img src="https://1.bp.blogspot.com/-BKFVhVVn-10/XURlzJ7Fc2I/AAAAAAAAAPk/6roIK4dZFLgfa61NdlhhLzuNa7hu5IV7wCLcBGAs/s1600/21.USM.jpg"
                                        alt="" style="height:250px;">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="float-end">
                                    <div id="qrcode"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <h2 class="fs-3 mb-3 text-decoration-underline">Detail User</h2>
                                <table class="table table-borderless">
                                    <tr>
                                        <th style="width: 150px;">Nama Mhs</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">NIM</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->nim }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Program Studi</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->program_studi->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Kelas</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->jenis_mhs }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Angkatan</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->angkatan }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Email</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->user->email }}</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <h2 class="fs-3 mb-3 text-decoration-underline">Detail Surat</h2>
                                <table class="table table-borderless">
                                    <tr>
                                        <th style="width: 150px;">No Surat</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->nomor }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Nama Surat</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->perihal }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Tanggal Ajuan</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <th style="width: 150px;">Tanggal Selesai</th>
                                        <td width="1">:</td>
                                        <td>{{ $surat->tanggal_selesai }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
    <script src="{{ asset('js/qrcode.min.js') }}"></script>
    <script>
        new QRCode(document.getElementById("qrcode"), {
            text: "<?= $link ?>",
            width: 120,
            height: 120,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });
    </script>
</body>

</html>
