<?php

namespace Database\Seeders;

use App\Models\SuratKelengkapan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuratKelengkapanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = ['nama_perusahaan', 'alamat'];
        foreach ($keys as $val) {
            SuratKelengkapan::create([
                'is_template'       => true,
                'jenis_surat_id'    => 1,
                'key'               => $val,
            ]);
        }
    }
}
