<?php

namespace Database\Seeders;

use App\Models\ProgramStudi;
use App\Models\StatusSurat;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // Program Studi
        $progdiArr = ['Teknik Informatika', 'Sistem Informasi', 'Ilmu Komunikasi'];
        foreach ($progdiArr as $item) {
            ProgramStudi::create([
                'nama' => $item
            ]);
        }

        $this->call([
            UserSeeder::class,
            SuratSeeder::class,
            SuratKelengkapanSeeder::class
        ]);
    }
}
