<?php

namespace Database\Seeders;

use App\Models\SuratJenis;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SuratSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // SuratJenis::
        $items = json_decode(file_get_contents(public_path('/json/surat.json')));

        foreach ($items as $item) {
            SuratJenis::create([
                'nama'          => $item->nama,
                'deskripsi'     => $item->deskripsi,
                'file'          => $item->file,
                'file_word'     => $item->file_word,
            ]);
        }
    }
}
