<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mahasiswa
        User::create([
            'nama' => 'Agus',
            'email' => 'agus@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Mahasiswa',
            'nim' => 'G.211.18.0001',
            'angkatan' => '2018',
            'jenis_mhs' => 'PAGI',
            'program_studi_id' => 1
        ]);
        User::create([
            'nama' => 'Jayan',
            'email' => 'jayan@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Mahasiswa',
            'nim' => 'G.211.18.0098',
            'angkatan' => '2018',
            'jenis_mhs' => 'PAGI',
            'program_studi_id' => 1
        ]);
        User::create([
            'nama' => 'Ayu',
            'email' => 'ayu@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Perempuan',
            'level' => 'Mahasiswa',
            'nim' => 'G.211.17.0024',
            'angkatan' => '2018',
            'jenis_mhs' => 'PAGI',
            'program_studi_id' => 2
        ]);

        // Dosen
        User::create([
            'nama' => 'KAPROGDI TI',
            'email' => 'kaprogditi@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Dosen',
            'nis' => '12345',
            'nidn' => '12345',
            'jabatan' => 'KAPROGDI TI'
        ]);
        User::create([
            'nama' => 'KAPROGDI SI',
            'email' => 'kaprogdisi@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Dosen',
            'nis' => '12345',
            'nidn' => '12345',
            'jabatan' => 'KAPROGDI SI'
        ]);
        User::create([
            'nama' => 'KAPROGDI ILKOM',
            'email' => 'kaprogdiilkom@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Dosen',
            'nis' => '12345',
            'nidn' => '12345',
            'jabatan' => 'KAPROGDI ILKOM'
        ]);
        User::create([
            'nama' => 'Dekan FTIK',
            'email' => 'dekan@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'Dosen',
            'nis' => '12345',
            'nidn' => '12345',
            'jabatan' => 'DEKAN'
        ]);

        // TU
        User::create([
            'nama' => 'Saiful',
            'email' => 'tuftik@gmail.com',
            'password' => Hash::make('password'),
            'jenis_kelamin' => 'Laki-Laki',
            'level' => 'TU',
        ]);
    }
}
