<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->string('nim')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('level')->nullable();
            $table->string('program_studi_id')->nullable();
            $table->string('semester')->nullable();
            $table->string('jenis_mhs')->nullable();
            $table->string('angkatan')->nullable();
            $table->string('jabatan')->nullable();
            $table->string('nis')->nullable();
            $table->string('nidn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
