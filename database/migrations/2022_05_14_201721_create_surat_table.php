<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat', function (Blueprint $table) {
            $table->id();
            $table->integer('pengirim_id')->nullable();
            $table->string('code')->nullable();
            $table->string('nomor')->nullable();
            $table->string('perihal')->nullable();
            $table->text('catatan')->nullable();
            $table->string('status_pengajuan')->nullable();
            $table->string('status_proses_paraf')->nullable();
            $table->string('tanggal_selesai')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat');
    }
};
