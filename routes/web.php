<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SuratController;
use App\Http\Livewire\Login;
use App\Http\Livewire\Logindk;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return to_route('login');
});

Route::middleware(['guest'])->group(function () {
    Route::get('/login', Login::class)->name('login');
    Route::get('/login/dk', Logindk::class)->name('login.dk');
});

// Route::get('/create', function () {
//     return json_decode(file_get_contents(public_path('/json/surat.json')), true);
// });
Route::get('/surat/overview/{code}', [SuratController::class, 'overview'])->name('surat.overview');
Route::get('/surat/mahasiswa/pdf/{id}', [SuratController::class, 'mahasiswa_pdf'])->name('surat.mahasiswa.pdf');

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [HomeController::class, 'index'])->name('home');

    Route::get('/surat', [SuratController::class, 'index'])->name('surat');
    Route::get('/surat/dosen', [SuratController::class, 'surat_dosen'])->name('surat.dosen');
    Route::get('/surat/dosen/word/{id}', [SuratController::class, 'surat_dosen_word'])->name('surat.dosen_word');
    Route::get('/surat/dosen/pdf/{id}', [SuratController::class, 'surat_dosen_pdf'])->name('surat.dosen_pdf');
    Route::get('/surat/informasi', [SuratController::class, 'informasi'])->name('surat.informasi');
    Route::get('/surat/tambah/mahasiswa', [SuratController::class, 'create'])->name('surat.tambah');
    Route::get('/surat/mahasiswa/word/{id}', [SuratController::class, 'surat_mhs_word'])->name('surat.mhs_word');
    Route::get('/surat/dosen/delete/{id}', [SuratController::class, 'delete_surat_dosen'])->name('surat.tambah.dosen');

    Route::get('/surat/kategori', [SuratController::class, 'surat_jenis'])->name('surat.jenis');
    Route::get('/surat/kategori/detail/{id}', [SuratController::class, 'surat_jenis_detail'])->name('surat.jenis_detail');
    Route::get('/surat/kategori/download/{id}', [SuratController::class, 'surat_jenis_download'])->name('surat.jenis_download');
    Route::get('/surat/kategori/delete/{id_jenis_surat}', [SuratController::class, 'surat_jenis_delete'])->name('surat.jenis_delete');

    Route::get('/surat/review-pdf/{id_surat}', [SuratController::class, 'review_pdf'])->name('surat.review_pdf');
    Route::get('/surat/download/kategori-surat/{id_jenis_surat}', [SuratController::class, 'download_jenis_surat'])->name('surat.download_jenis_surat');
    Route::get('/surat/download/surat/{id_surat}', [SuratController::class, 'download_surat'])->name('surat.download_surat');
    Route::get('/surat/ttd/{id_surat}', [SuratController::class, 'ttd_surat'])->name('surat.ttd_surat');
    Route::get('/surat/detail/{id_surat}', [SuratController::class, 'detail_surat'])->name('surat.detail_surat');
    Route::get('/surat/edit/{id_surat}', [SuratController::class, 'edit_surat'])->name('surat.edit_surat');
    Route::get('/surat/delete/{id_surat}', [SuratController::class, 'delete'])->name('surat.delete');
    Route::post('/surat', [SuratController::class, 'store'])->name('surat.store');
    Route::post('/surat/dosen', [SuratController::class, 'store_dosen'])->name('surat.store_dosen');
    Route::post('/surat/kategori', [SuratController::class, 'surat_jenis_store'])->name('surat.jenis_store');
    Route::patch('/surat/update', [SuratController::class, 'update'])->name('surat.update');
    Route::patch('/surat/kategori', [SuratController::class, 'surat_jenis_update'])->name('surat.jenis_update');

    Route::get('/activity-log', [HomeController::class, 'activity_log'])->name('activity_log');

    Route::get('/logout', [HomeController::class, 'logout'])->name('logout');
});
