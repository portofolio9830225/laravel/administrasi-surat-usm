<?php

namespace App\Http\Livewire\Mahasiswa;

use App\Models\FileSurat;
use App\Models\Surat;
use App\Models\SuratJenis;
use App\Models\SuratKelengkapan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\WithFileUploads;
use Livewire\Component;

class SuratTambah extends Component
{
    use WithFileUploads;

    public $penerima = 'TATA USAHA FTIK USM';
    public $perihal, $file;

    public function render()
    {
        if ($this->perihal == '') {
            $isi = [];
            $deskripsi = '';
        } else {
            $suratJenis = SuratJenis::where('nama', $this->perihal)->first();
            $isi = SuratKelengkapan::where('jenis_surat_id', $suratJenis->id)->where('is_template', 1)->get();
            $deskripsi = $suratJenis->deskripsi;
        }

        return view('livewire.mahasiswa.surat-tambah', [
            'surat'     => SuratJenis::latest()->get(),
            'isi'       => $isi,
            'deskripsi' => $deskripsi
        ]);
    }
}
