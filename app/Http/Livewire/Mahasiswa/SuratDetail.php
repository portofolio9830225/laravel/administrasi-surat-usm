<?php

namespace App\Http\Livewire\Mahasiswa;

use Livewire\Component;

class SuratDetail extends Component
{
    public $surat;
    public $perihal;

    public function mount()
    {
        $this->perihal = $this->surat->perihal;
    }

    public function render()
    {
        return view('livewire.mahasiswa.surat-detail');
    }

    public function download()
    {
        $pathToFile = public_path('file/surat/' . $this->surat->file->file_nama);
        return response()->download($pathToFile);
    }
}
