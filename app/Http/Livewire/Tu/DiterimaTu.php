<?php

namespace App\Http\Livewire\Tu;

use App\Models\Surat;
use App\Models\SuratTtd;
use App\Models\User;
use Livewire\Component;

class DiterimaTu extends Component
{
    public $idsurat, $ajuan_ttd, $nomor;

    // protected $listeners = ['revisiSuratMahasiswa'];

    public function render()
    {
        $user   = User::where('jabatan', 'DEKAN')->first();
        $is_pdf = SuratTtd::where('ttd', 1)->where('user_id', $user->id)->where('surat_id', $this->idsurat)->first();

        return view('livewire.tu.diterima-tu', [
            'ttd'       => SuratTtd::with('user')->where('surat_id', $this->idsurat)->get(),
            'is_pdf'    => !is_null($is_pdf) ? true : false,
            'surat'     => Surat::find($this->idsurat)
        ]);
    }

    public function mintaParaf()
    {
        // dd($this->ajuan_ttd);
        $user   = User::where('jabatan', $this->ajuan_ttd)->first();
        $surat  = Surat::find($this->idsurat);

        Surat::find($surat->id)->update([
            'status_pengajuan'      => 'Proses Paraf',
            'status_proses_paraf'   => 'Proses Paraf ' . $this->ajuan_ttd
        ]);

        SuratTtd::create([
            'user_id'   => $user->id,
            'surat_id'  => $surat->id,
        ]);

        activity()->log($surat->perihal . ' Sedang Proses TandaTangan ' . $this->ajuan_ttd);
        $this->emit('parafSurat');

        $this->ajuan_ttd = '';
    }

    public function deleteParaf($id_paraf)
    {
        $suratTtd = SuratTtd::where('surat_id', $this->idsurat)->count();

        if ($suratTtd == 1) {
            Surat::find($this->idsurat)->update([
                'catatan'               => null,
                'status_pengajuan'      => 'Diterima TU',
                'status_proses_paraf'   => null
            ]);
            $this->emit('deleteParaf');
        }
        SuratTtd::destroy($id_paraf);
    }

    // public function revisiSuratMahasiswa()
    // {
    // }

    public function generatePdf()
    {
        if ($this->nomor != '') {
            Surat::find($this->idsurat)->update([
                'status_pengajuan'  => 'Selesai',
                'tanggal_selesai'   => now(),
                'nomor'             => $this->nomor,
                'code'              => generateRandomString(5)
            ]);

            $this->emit('generatePdf');
        }
    }
}
