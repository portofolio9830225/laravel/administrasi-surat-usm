<?php

namespace App\Http\Livewire\Tu;

use App\Models\Surat;
use Livewire\Component;

class MenungguKonfirmasi extends Component
{
    public $paraf = '';
    public $catatan, $idsurat;
    public $isProses = false;

    protected $listeners = ['revisiSuratMahasiswa'];

    public function render()
    {
        return view('livewire.tu.menunggu-konfirmasi');
    }

    public function saveAjuanSurat()
    {
        if ($this->paraf != '') {
            $this->isProses = true;
            $surat = Surat::find($this->idsurat);

            $surat->update([
                'status_pengajuan'  => $this->paraf,
                'catatan'           => $this->catatan,
            ]);

            activity()->log($surat->perihal . ' ' . $surat->paraf);
            $this->emit('handleAjuanSurat');
        }
    }

    public function revisiSuratMahasiswa()
    {
    }
}
