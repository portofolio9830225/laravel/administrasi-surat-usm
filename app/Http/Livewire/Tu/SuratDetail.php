<?php

namespace App\Http\Livewire\Tu;

use App\Models\Surat;
use App\Models\SuratTtd;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class SuratDetail extends Component
{
    public $idsurat, $pdf;
    public $perihal, $status_surat, $catatan, $nomor, $ajuan_ttd;

    protected $listeners = ['handleAjuanSurat', 'parafSurat', 'generatePdf', 'deleteParaf'];

    public function render()
    {
        return view('livewire.tu.surat-detail', [
            'surat' => Surat::with('file', 'user', 'tanda_tangan_all')->where('id', $this->idsurat)->first(),
            'paraf' => SuratTtd::where('surat_id', $this->idsurat)->count()
        ]);
    }

    public function handleAjuanSurat()
    {
    }

    public function parafSurat()
    {
    }

    public function backMahasiswa()
    {
        // dd($this->idsurat);
        DB::beginTransaction();
        $surat                          = Surat::find($this->idsurat);
        $surat->status_pengajuan        = 'Revisi';
        $surat->save();

        SuratTtd::where('surat_id', $this->idsurat)->delete();
        DB::commit();

        return to_route('surat')->with('status', 'Revisi Surat Mahasiswa Berhasil');
    }

    public function generatePdf()
    {
    }

    public function deleteParaf()
    {
    }
}
