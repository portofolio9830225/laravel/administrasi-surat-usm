<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Http\Request;

class Logindk extends Component
{
    public $email, $password;

    protected $rules = [
        'email'     => 'required',
        'password'  => 'required',
    ];

    protected $messages = [
        'email.required'    => 'Email wajib diisi.',
        'password.required' => 'Password wajib diisi.',
    ];

    public function render()
    {
        return view('livewire.logindk')->layout('layout.livewire');
    }

    public function submit()
    {
        $this->validate();

        $auth = [
            'email'     => $this->email,
            'password'  => $this->password
        ];

        if (Auth::attempt($auth)) {
            request()->session()->regenerate();
            activity()->log('Berhasil Login with ' . get_client_browser());

            return to_route('home')->with('status', 'Anda berhasil login !!!');
        }

        return back()->with('msg', 'Email / Password Salah');
    }
}
