<?php

namespace App\Http\Livewire\Dosen;

use App\Models\SuratTtd;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class SuratDetail extends Component
{
    public $surat;
    public $perihal;

    protected $listeners = ['handleCatatanDosen'];

    public function mount()
    {
        $this->perihal = $this->surat->perihal;
    }

    public function render()
    {
        return view('livewire.dosen.surat-detail', [
            'paraf' => SuratTtd::where('surat_id', $this->surat->id)->where('user_id', Auth::id())->first()
        ]);
    }

    public function handleCatatanDosen()
    {
    }
}
