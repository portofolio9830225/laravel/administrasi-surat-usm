<?php

namespace App\Http\Livewire\Dosen;

use App\Models\Surat;
use App\Models\SuratTtd;
use Livewire\Component;

class Catatan extends Component
{
    public $catatan, $idsurat, $ajuan_paraf;

    public function render()
    {
        return view('livewire.dosen.catatan');
    }

    public function submit()
    {
        if (!is_null($this->catatan)) {
            Surat::find($this->idsurat)->update([
                'catatan'               => $this->catatan,
                'status_pengajuan'      => 'Revisi',
                'nomor'                 => null
            ]);

            SuratTtd::where('surat_id', $this->idsurat)->delete();

            activity()->log('Beri catatan surat');

            return to_route('surat')->with('status', 'Beri Catatan Surat Berhasil');
        }
    }

    public function ajuanDiterima()
    {
        dd($this->ajuan_paraf);
    }
}
