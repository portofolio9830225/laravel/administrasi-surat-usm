<?php

namespace App\Http\Livewire;

use App\Models\ProgramStudi;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Login extends Component
{
    public $layout = 'login';
    public $nim, $password, $namaRegister, $nimRegister, $jenisKelamin, $prodi_id, $jenis_mhs, $email, $password2, $password_konfirmasi;

    protected $rules = [
        'nim'       => 'required',
        'password'  => 'required',
    ];

    protected $messages = [
        'nim.required'      => 'Nim wajib diisi.',
        'password.required' => 'Password wajib diisi.',
    ];

    public function render()
    {
        return view('livewire.login', [
            'prodi' => ProgramStudi::get()
        ])->layout('layout.livewire');
    }

    public function submit()
    {
        $this->validate();

        $user = User::where('nim', $this->nim)
            ->where('level', 'Mahasiswa')
            ->first();

        if (is_null($user)) {
            return back()->with('msg', 'NIM / Password Salah');
        }

        $auth = [
            'email'     => $user->email,
            'password'  => $this->password
        ];

        if (Auth::attempt($auth)) {
            request()->session()->regenerate();
            activity()->log('Berhasil Login with ' . get_client_browser());

            return to_route('home')->with('msg', 'Anda Berhasil Login');
        }

        return back()->with('msg', 'NIM / Password Salah');
    }

    public function handleLayout()
    {
        if ($this->layout == 'login') {
            $this->layout = 'register';
        } else {
            $this->layout = 'login';
        }
    }

    public function register()
    {
        $this->validate([
            'email'     => 'unique:users,email',
            'password2' => 'min:6|same:password_konfirmasi',
        ], [
            'email.unique'      => 'Email sudah terdaftar',
            'password2.same'    => 'Password tidak sama',
            'password2.min'     => 'Password minimal 6 karakter',
        ]);

        $arrNim = explode('.', $this->nimRegister);

        User::create([
            'nama'              => $this->namaRegister,
            'nim'               => $this->nimRegister,
            'email'             => $this->email,
            'password'          => Hash::make($this->password2),
            'jenis_kelamin'     => $this->jenisKelamin,
            'level'             => 'Mahasiswa',
            'program_studi_id'  => $this->prodi_id,
            'jenis_mhs'         => $this->jenis_mhs,
            'angkatan'          => '20' . $arrNim[2]
        ]);

        $auth = [
            'email'     => $this->email,
            'password'  => $this->password2
        ];

        if (Auth::attempt($auth)) {
            request()->session()->regenerate();
            activity()->log('Berhasil Login with ' . get_client_browser());

            return to_route('home')->with('msg', 'Anda Berhasil Login');
        }
    }
}
