<?php

namespace App\Http\Controllers;

use App\Models\ActivityLog;
use App\Models\Surat;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;

class HomeController extends Controller
{
    public function index()
    {
        if (Auth::user()->level == 'Mahasiswa') {
            return $this->index_mahasiswa();
        } elseif (Auth::user()->level == 'TU') {
            return $this->index_tu();
        } elseif (Auth::user()->level == 'Dosen') {
            return $this->index_dosen();
        }
    }

    public function index_mahasiswa()
    {
        $data['logs']           = ActivityLog::where('causer_id', Auth::id())->limit(10)->latest()->get();
        $data['diterima']       = Surat::where('status_pengajuan', 'Diterima TU')->count();
        $data['revisi']         = Surat::where('status_pengajuan', 'Revisi')->count();
        $data['ditolak']        = Surat::where('status_pengajuan', 'Ditolak TU')->count();
        $data['menunggu']       = Surat::where('status_pengajuan', 'Menunggu Konfirmasi')->count();
        $data['selesai']        = Surat::where('status_pengajuan', 'Selesai')->count();
        $data['paraf']          = Surat::where('status_pengajuan', 'Proses Paraf')->count();

        return view('mahasiswa.home', $data);
    }

    public function index_tu()
    {
        $data['logs']           = ActivityLog::where('causer_id', Auth::id())->limit(10)->latest()->get();
        $data['diterima']       = Surat::where('status_pengajuan', 'Diterima TU')->count();
        $data['revisi']         = Surat::where('status_pengajuan', 'Revisi')->count();
        $data['ditolak']        = Surat::where('status_pengajuan', 'Ditolak TU')->count();
        $data['menunggu']       = Surat::where('status_pengajuan', 'Menunggu Konfirmasi')->count();
        $data['selesai']        = Surat::where('status_pengajuan', 'Selesai')->count();
        $data['paraf']          = Surat::where('status_pengajuan', 'Proses Paraf')->count();

        return view('tu.home', $data);
    }

    public function index_dosen()
    {
        $data['logs']           = ActivityLog::where('causer_id', Auth::id())->limit(10)->latest()->get();
        $data['diterima']       = Surat::where('status_pengajuan', 'Diterima TU')->count();
        $data['revisi']         = Surat::where('status_pengajuan', 'Revisi')->count();
        $data['ditolak']        = Surat::where('status_pengajuan', 'Ditolak TU')->count();
        $data['menunggu']       = Surat::where('status_pengajuan', 'Menunggu Konfirmasi')->count();
        $data['selesai']        = Surat::where('status_pengajuan', 'Selesai')->count();
        $data['paraf']        = Surat::where('status_pengajuan', 'Proses Paraf')->count();

        return view('dosen.home', $data);
    }

    public function logout()
    {
        if (Auth::user()->level == 'Mahasiswa') {
            Auth::logout();
            return redirect()->route('login');
        } elseif (Auth::user()->level == 'Dosen' || Auth::user()->level == 'TU') {
            Auth::logout();
            return redirect()->route('login.dk');
        }
    }

    public function activity_log()
    {
        if (request()->ajax()) {
            $data = ActivityLog::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('created_at', function ($row) {
                    return $row->created_at->diffForHumans();
                })
                ->make(true);
        }
        // return $activitys;
        return view('tu.activityLog');
    }
}
