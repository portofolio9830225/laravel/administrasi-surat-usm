<?php

namespace App\Http\Controllers;

use App\Models\FileSurat;
use App\Models\Surat;
use App\Models\SuratDosen;
use App\Models\SuratJenis;
use App\Models\SuratKelengkapan;
use App\Models\SuratTtd;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DataTables;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use WordTemplate;

class SuratController extends Controller
{
    public function index()
    {
        if (Auth::user()->level == 'Mahasiswa') {
            return $this->index_mahasiswa();
        } elseif (Auth::user()->level == 'TU') {
            return $this->index_tu();
        } elseif (Auth::user()->level == 'Dosen') {
            return $this->index_dosen();
        }
    }

    public function index_mahasiswa()
    {
        if (request()->ajax()) {
            $data = Surat::where('pengirim_id', Auth::id())->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('nomor', function ($row) {
                    if (is_null($row->nomor)) {
                        return '---';
                    } else {
                        return $row->nomor;
                    }
                })
                ->editColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->editColumn('status_pengajuan', function ($row) {
                    if ($row->status_pengajuan == 'Diterima TU' || $row->status_pengajuan == 'Selesai') {
                        return '<span class="badge bg-success">' . $row->status_pengajuan . '</span>';
                    } elseif ($row->status_pengajuan == 'Ditolak TU') {
                        return '<span class="badge bg-danger">' . $row->status_pengajuan . '</span>';
                    } elseif ($row->status_pengajuan == 'Proses Paraf') {
                        return '<span class="badge bg-primary">' . $row->status_proses_paraf . '</span>';
                    } else {
                        return '<span class="badge bg-warning text-dark">' . $row->status_pengajuan . '</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $action = '';
                    $action .= '<button type="button" onclick=window.location="' . route('surat.detail_surat', enc($row->id)) . '" class="btn btn-sm btn-info me-1"><i class="fas fa-eye"></i></button>';
                    if ($row->status_pengajuan == 'Revisi') {
                        $action .= '<button type="button" onclick=window.location="' . route('surat.edit_surat', enc($row->id)) . '" class="btn btn-sm btn-warning me-1"><i class="fas fa-edit"></i></button>';
                    }
                    $action .= '<button type="button" class="btn btn-sm btn-danger" onclick="handleDelete(' . $row->id . ')"><i class="fas fa-trash"></i></button>';
                    return $action;
                })
                ->rawColumns(['nomor', 'status_pengajuan', 'action'])
                ->make(true);
        }

        return view('mahasiswa.surat');
    }

    public function index_tu()
    {
        if (request()->ajax()) {
            $data = Surat::with('user')->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('nomor', function ($row) {
                    if (is_null($row->nomor)) {
                        return '---';
                    } else {
                        return $row->nomor;
                    }
                })
                ->editColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->editColumn('status_pengajuan', function ($row) {
                    if ($row->status_pengajuan == 'Diterima TU' || $row->status_pengajuan == 'Selesai') {
                        return '<span class="badge bg-success">' . $row->status_pengajuan . '</span>';
                    } elseif ($row->status_pengajuan == 'Ditolak TU') {
                        return '<span class="badge bg-danger">' . $row->status_pengajuan . '</span>';
                    } elseif ($row->status_pengajuan == 'Proses Paraf') {
                        return '<span class="badge bg-primary">' . $row->status_proses_paraf . '</span>';
                    } else {
                        return '<span class="badge bg-warning text-dark">' . $row->status_pengajuan . '</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $action = '';
                    $action .= '<button type="button" onclick=window.location="' . route('surat.detail_surat', Crypt::encrypt($row->id)) . '" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></button>';
                    return $action;
                })
                ->rawColumns(['nomor', 'status_pengajuan', 'action'])
                ->make(true);
        }

        $data['active'] = 'Surat Mahasiswa';

        return view('tu.surat', $data);
    }

    public function index_dosen()
    {
        // return Surat::with('user', 'tanda_tangan')->where('status_pengajuan', 'Proses Paraf')->latest()->get();
        // return SuratTtd::with('surat', 'user')->where('user_id', Auth::id())->latest()->get();
        if (request()->ajax()) {
            $data = SuratTtd::with('surat', 'user')->where('user_id', Auth::id())->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('surat.nomor', function ($row) {
                    if (is_null($row->surat->nomor)) {
                        return '---';
                    } else {
                        return $row->surat->nomor;
                    }
                })
                ->editColumn('created_at', function ($row) {
                    return $row->created_at;
                })
                ->editColumn('ttd', function ($row) {
                    if ($row->ttd == 0) {
                        return '<span class="badge rounded-pill bg-warning text-dark">Proses Paraf</span>';
                    } elseif ($row->ttd == 1) {
                        return '<span class="badge rounded-pill bg-success">Sudah Paraf</span>';
                    }
                })
                ->addColumn('action', function ($row) {
                    $action = '';
                    $action .= '<button type="button" onclick=window.location="' . route('surat.detail_surat', Crypt::encrypt($row->surat->id)) . '" class="btn btn-sm btn-info"><i class="fas fa-eye"></i></button>';
                    return $action;
                })
                ->rawColumns(['surat.nomor', 'ttd', 'action'])
                ->make(true);
        }

        $data['active'] = 'Surat Mahasiswa';

        return view('dosen.surat', $data);
    }

    public function surat_dosen()
    {
        if (Auth::user()->level == 'TU') {
            if (request()->ajax()) {
                $data = SuratDosen::with('user')->latest()->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return $row->created_at;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        $action .= '<button type="button" onclick=window.location="' . route('surat.dosen_pdf', enc($row->id)) . '" class="btn btn-sm btn-primary me-1"><i class="fas fa-file-pdf"></i></button>';
                        $action .= '<button type="button" onclick=handleDelete(' . $row->id . ') class="btn btn-sm btn-danger me-1"><i class="fas fa-trash"></i></button>';

                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
            $data['active'] = 'Surat Dosen';
            $data['dosen']  = User::where('level', 'dosen')->get();

            return view('tu.suratDosen', $data);
        } elseif (Auth::user()->level == 'Dosen') {
            if (request()->ajax()) {
                $data = SuratDosen::with('user')->where('pengirim_id', Auth::id())->latest()->get();
                return Datatables::of($data)
                    ->addIndexColumn()
                    ->editColumn('created_at', function ($row) {
                        return $row->created_at;
                    })
                    ->addColumn('action', function ($row) {
                        $action = '';
                        $action .= '<button type="button" onclick=window.location="' . route('surat.dosen_pdf', enc($row->id)) . '" class="btn btn-sm btn-primary"><i class="fas fa-file-pdf"></i></button>';

                        return $action;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }
            $data['active'] = 'Surat Dosen';

            return view('dosen.suratDosen', $data);
        }
    }

    public function surat_dosen_word($id)
    {
        $file       = public_path('surat_riset.rtf');
        $nama_file  = 'surat_riset.doc';
        $surat      = SuratDosen::with('user')->find(dec($id));
        // return $surat;
        $array = array(
            '[PERIHAL]' => $surat->perihal,
            '[NAMA]'    => \Str::upper($surat->user->nama)
        );

        return WordTemplate::export($file, $array, $nama_file);
    }

    public function surat_dosen_pdf($id)
    {
        $suratDosen = SuratDosen::find(dec($id));
        $pathToFile = public_path('file/surat/' . $suratDosen->file);
        return response()->download($pathToFile);
    }

    public function create()
    {
        return view('mahasiswa.suratTambah');
    }

    public function surat_mhs_word($id)
    {
        // $file       = public_path('file/jenis-surat/surat_permohonan_kp.rtf');
        $file       = public_path('surat_permohonan_kp.rtf');
        $nama_file  = 'surat_permohonan_kp.doc';
        $surat      = Surat::with('user', 'surat_kelengkapan')->find($id);
        // return $surat;

        $array = array(
            // '[PERIHAL]' => $surat->perihal,
            '[NAMA]'        => $surat->user->nama,
            '[NIM]'         => $surat->user->nim,
            '[PRODI]'       => $surat->user->program_studi->nama,
            '[PIMPINAN]'    => $surat->surat_kelengkapan[0]->value,
            '[ALAMAT]'      => $surat->surat_kelengkapan[1]->value,
            '[PIMPINAN2]'   => $surat->surat_kelengkapan[0]->value,
        );

        return WordTemplate::export($file, $array, $nama_file);
    }

    public function delete_surat_dosen($id)
    {
        SuratDosen::destroy($id);
        return back()->with('status', 'Surat Dosen berhasil dihapus');
    }

    public function store()
    {
        // return request();
        DB::beginTransaction();
        $file = request()->file('file');
        $nama = time() . '' . Auth()->user()->id . '.' . $file->getClientOriginalExtension();
        $file->move('file/surat/', $nama);

        $jenisSurat = SuratJenis::where('nama', request()->perihal)->first();
        $suratKelengkapan = SuratKelengkapan::where('jenis_surat_id', $jenisSurat->id)->where('is_template', 1)->pluck('key');

        $data = [];
        for ($i = 0; $i < count($suratKelengkapan); $i++) {
            $data[] = [
                'key'       => $suratKelengkapan[$i],
                'value'     => request()->key[$i],
            ];
        }
        // return $data;
        $surat = Surat::create([
            'pengirim_id'       => Auth::id(),
            'perihal'           => request()->perihal,
            'status_pengajuan'  => "Menunggu Konfirmasi"
        ]);

        foreach ($data as $val) {
            SuratKelengkapan::create([
                'is_template'       => 0,
                'jenis_surat_id'    => $jenisSurat->id,
                'surat_id'          => $surat->id,
                'key'               => $val['key'],
                'value'             => $val['value'],
            ]);
        }

        FileSurat::create([
            'surat_id'  => $surat->id,
            'file_link' => asset('file/surat/' . $nama),
            'file_nama' => $nama,
        ]);
        DB::commit();

        activity()->log('Ajukan ' . request()->perihal);
        return redirect('surat')->with('status', 'Ajuan Surat Berhasil dibuat');
    }

    public function store_dosen()
    {
        $file = request()->file('file');
        $nama = time() . Auth()->user()->id . '.' . $file->getClientOriginalExtension();
        $file->move('file/surat/', $nama);

        SuratDosen::create([
            'pengirim_id'   => request()->penerima,
            'nomor'         => request()->nomor,
            'perihal'       => request()->perihal,
            'file'          => $nama
        ]);

        return back()->with('status', 'Surat Dosen berhasil dibuat');
    }

    public function surat_jenis_store()
    {
        // return request();
        DB::beginTransaction();
        // $file = request()->file('file');
        // $nama = time() . '' . Auth()->user()->id . '.' . $file->getClientOriginalExtension();
        // $file->move('file/jenis-surat/', $nama);

        SuratJenis::create([
            'nama'          => request()->nama,
            'deskripsi'     => request()->deskripsi,
            // 'file'          => $nama,
        ]);

        activity()->log(request()->nama . ' berhasil ditambah');
        DB::commit();
        return to_route('surat.jenis')->with('status', 'Jenis Surat Berhasil ditambah');
    }

    public function surat_jenis_detail($id)
    {
        $surat_jenis = SuratJenis::find($id);
        return response()->json($surat_jenis);
    }

    public function surat_jenis_download($id)
    {
        $suratJenis = SuratJenis::find($id);
        $pathToFile = public_path('file/jenis-surat/' . $suratJenis->file);
        return response()->download($pathToFile);
    }

    public function surat_jenis_delete($id_jenis_surat)
    {
        DB::beginTransaction();
        $jenis_surat = SuratJenis::find($id_jenis_surat);
        activity()->log(request()->nama . ' berhasil dihapus');
        \File::delete('file/jenis-surat/' . $jenis_surat->file);
        $jenis_surat->delete();
        DB::commit();

        return to_route('surat.jenis')->with('status', 'Jenis Surat Berhasil dihapus');
    }

    public function delete($id_surat)
    {
        // return 123;
        DB::beginTransaction();
        $surat = Surat::find($id_surat);
        activity()->log('Delete ' . $surat->perihal);
        $surat->delete();

        SuratTtd::where('surat_id', $id_surat)->delete();
        SuratKelengkapan::where('surat_id', $id_surat)->delete();

        $fileSurat = FileSurat::where('surat_id', $id_surat)->first();
        File::delete('file/surat/' . $fileSurat->file_nama);
        $fileSurat->delete();
        DB::commit();

        return to_route('surat')->with('status', 'Surat Berhasil dihapus');
    }

    public function detail_surat($id_surat)
    {
        $data['surat'] = Surat::with('file', 'user', 'tanda_tangan_all', 'surat_kelengkapan')->where('id', Crypt::decrypt($id_surat))->first();
        // return $data['surat'];

        $code = is_null($data['surat']->code) ? $data['surat']->id : $data['surat']->code;

        if (Auth::user()->level == 'Mahasiswa') {

            $data['link']   = route('surat.overview', $code);
            return view('mahasiswa.suratDetail', $data);
        } elseif (Auth::user()->level == 'TU') {
            $pdf = [];
            foreach ($data['surat']->tanda_tangan_all as $item) {
                if ($item->ttd == 0) {
                    $pdf[] = $item->ttd;
                }
            }

            $data['pdf']    = count($pdf);
            $data['id']     = Crypt::decrypt($id_surat);
            $data['link']   = route('surat.overview', $code);

            // dd($data);
            return view('tu.suratDetail', $data);
        } else {
            return view('dosen.suratDetail', $data);
        }
    }

    public function edit_surat($id_surat)
    {
        $data['surat']  = Surat::with('surat_kelengkapan')->find(dec($id_surat));
        $data['jenis']  = SuratJenis::latest()->get();
        return view('mahasiswa.suratEdit', $data);
    }

    public function ttd_surat($id_surat)
    {
        SuratTtd::where('user_id', Auth::id())->where('surat_id', $id_surat)->update([
            'ttd'   => SuratTtd::SUDAH_TTD
        ]);

        $surat = Surat::find($id_surat);
        $surat->update([
            'status_proses_paraf'   => 'ACC ' . Auth::user()->jabatan
        ]);
        // return $surat;

        activity()->log($surat->perihal . ' Sudah TandaTangan ' . Auth::user()->jabatan);
        return to_route('surat')->with('status', 'Berhasil TandaTangan');
    }

    public function informasi()
    {
        $surat = SuratJenis::where('nama', request()->nama)->first();
        return response()->json($surat);
        // $data['informasi'] = SuratJenis::get();
        // return view('mahasiswa.suratInformasi', $data);
    }

    public function update()
    {
        // return request();
        DB::beginTransaction();
        $surat = Surat::with('file')->find(request()->id_surat);
        \File::delete('file/surat/' . $surat->file->file_nama);

        $file = request()->file('file');
        $nama = time() . '' . Auth()->user()->id . '.' . $file->getClientOriginalExtension();
        $file->move('file/surat/', $nama);

        $surat->file->update([
            'file_link' => asset('file/surat' . $nama),
            'file_nama' => $nama
        ]);
        $jenisSurat = SuratJenis::where('nama', request()->nama_jns_surat)->first();
        $suratKelengkapan = SuratKelengkapan::where('jenis_surat_id', $jenisSurat->id)->where('is_template', 1)->pluck('key');

        $data = [];
        for ($i = 0; $i < count($suratKelengkapan); $i++) {
            $data[] = [
                'key'       => $suratKelengkapan[$i],
                'value'     => request()->key[$i],
            ];
        }
        $suratKelengkapan2 = SuratKelengkapan::where('jenis_surat_id', $jenisSurat->id)->where('is_template', 0)->where('surat_id', $surat->id)->get();
        // return $suratKelengkapan2;

        for ($i = 0; $i < count($suratKelengkapan2); $i++) {
            SuratKelengkapan::find($suratKelengkapan2[$i]['id'])->update([
                'value' => $data[$i]['value']
            ]);
        }

        $surat->update([
            'status_pengajuan'  => 'Menunggu Konfirmasi',
            'catatan'           => ''
        ]);
        DB::commit();

        activity()->log('Berhasil Update Surat');

        return to_route('surat')->with('status', 'Surat Berhasil diupdate');
    }

    public function surat_jenis_update()
    {
        // return request();
        DB::beginTransaction();
        $jenis_surat = SuratJenis::find(request()->id);
        $jenis_surat->nama      = request()->nama;
        $jenis_surat->deskripsi = request()->deskripsi;

        if (request()->hasFile('file')) {
            \File::delete('file/jenis-surat/' . $jenis_surat->file);
            $file = request()->file('file');
            $nama = time() . '' . Auth()->user()->id . '.' . $file->getClientOriginalExtension();
            $file->move('file/jenis-surat/', $nama);

            $jenis_surat->file = $nama;
        }

        $jenis_surat->save();

        activity()->log(request()->nama . ' berhasil diupdate');
        DB::commit();
        return to_route('surat.jenis')->with('status', 'Jenis Surat Berhasil diupdate');
    }

    public function surat_jenis()
    {
        if (request()->ajax()) {
            $data = SuratJenis::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                // ->editColumn('file', function ($row) {
                //     return '<button type="button" class="btn btn-sm btn-outline-primary" onclick=window.location="' . route('surat.jenis_download', $row->id) . '"><i class="fas fa-file-download"></i>&nbsp;Download</button>';
                // })
                ->addColumn('action', function ($row) {
                    $aksi = '';
                    $aksi .= '<button type="button" class="btn btn-sm btn-warning me-1" onclick="handleEdit(' . $row->id . ')"><i class="fas fa-edit"></i>&nbsp;Edit</button>';
                    $aksi .= '<button type="button" class="btn btn-sm btn-danger" onclick="handleDelete(' . $row->id . ')"><i class="fas fa-trash"></i>&nbsp;Delete</button>';
                    return $aksi;
                })
                ->rawColumns(['file', 'action'])
                ->make(true);
        }

        return view('tu.jenisSurat');
    }

    public function download_surat($id_surat)
    {
        $surat = Surat::with('file')->find($id_surat);
        $pathToFile = public_path('file/surat/' . $surat->file->file_nama);
        return response()->download($pathToFile);
    }

    public function review_pdf($id_surat)
    {
        $surat = Surat::with('file')->find($id_surat);
        // $pathToFile = public_path('file/surat/' . $surat->file->file_nama);

        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=" . $surat->file->file_nama);
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        @readfile('file/surat/' . $surat->file->file_nama);
    }

    public function overview($code)
    {
        $data['surat']  = Surat::with('user', 'tanda_tangan_all', 'tanda_tangan_all.user')->where('code', $code)->first();
        $data['link']   = route('surat.mahasiswa.pdf', $data['surat']->id);

        return view('overview', $data);
    }

    public function mahasiswa_pdf($id)
    {
        $data['surat'] = Surat::with('file', 'user', 'tanda_tangan_all', 'surat_kelengkapan')->find($id);
        // return $data;
        return view('mahasiswa.pdf', $data);
    }
}
