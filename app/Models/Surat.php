<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Surat extends Model
{
    use HasFactory;
    protected $table = 'surat';
    protected $guarded = ['id'];

    public function file()
    {
        return $this->belongsTo(FileSurat::class, 'id', 'surat_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'pengirim_id');
    }

    public function tanda_tangan()
    {
        return $this->belongsTo(SuratTtd::class, 'id', 'surat_id');
    }

    public function tanda_tangan_all()
    {
        return $this->hasMany(SuratTtd::class);
    }

    public function surat_kelengkapan()
    {
        return $this->hasMany(SuratKelengkapan::class);
    }
}
