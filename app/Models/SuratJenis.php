<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratJenis extends Model
{
    use HasFactory;
    protected $table = 'jenis_surat';
    protected $guarded = ['id'];
}
