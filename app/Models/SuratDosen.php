<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratDosen extends Model
{
    use HasFactory;
    protected $table = 'surat_dosen';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'pengirim_id');
    }
}
