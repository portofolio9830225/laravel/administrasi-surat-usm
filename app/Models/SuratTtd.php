<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SuratTtd extends Model
{
    use HasFactory;
    protected $table = 'surat_ttd';
    protected $guarded = ['id'];

    const BELUM_TTD = 0;
    const SUDAH_TTD = 1;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function surat()
    {
        return $this->belongsTo(Surat::class, 'surat_id');
    }
}
